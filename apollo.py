# This script will download the VirusShare torrent archives via a hacky wrapper around the Deluge 1.x console
# then decompress the them and filter the resulting files.
# The script cannot handle spaces in the path due to a bug in python-magic! 
# A method is availible to avoid triggering this, but it doesn't always work.


import os
import time
import magic #pip3 install python-libmagic
import glob
import pathlib
import shutil
import platform
import libarchive # pip3 install libarchive-c
import subprocess

# Set commonly used paths to variables.
working_directory = pathlib.Path(__file__).parent.absolute()
torrents_directory = str(pathlib.Path(str(working_directory) + "/torrents"))
completed_torrents = str(pathlib.Path(str(working_directory) + "/completed_torrents"))
downloads_directory = str(pathlib.Path(str(working_directory) + "/downloads"))
extracted_directory = str(pathlib.Path(str(working_directory) + "/extracted"))
mac_samples_directory = str(pathlib.Path(str(working_directory) + "/mac-samples"))
linux_samples_directory = str(pathlib.Path(str(working_directory) + "/linux-samples"))
deluge_directory = str(pathlib.Path(str(working_directory) + "/Deluge.app/Contents/MacOS/"))
scripts_directory = str(pathlib.Path(str(working_directory) + "/scripts"))

# If OS is not macOS, then set the deluge_directory variable to an empty string.
# We prefix commands in the script for macOS hosts, but not for Linux ones as deluge will be in the PATH.
if ("macOS" in str(platform.platform())):
    print("Detected macOS host. Prefixing Deluge commands for local app bundle.")
else:
    deluge_directory = ""

# Create directories as needed for script.
os.system("mkdir " + torrents_directory)
os.system("mkdir " + completed_torrents)
os.system("mkdir " + downloads_directory)
os.system("mkdir " + extracted_directory)
os.system("mkdir " + mac_samples_directory)
os.system("mkdir " + linux_samples_directory)


def download_torrent_data(torrent_file_name):
    # Start the Deluge daemon. If one is already running that is OK, this wont hurt anything.
    os.system(deluge_directory + "deluged")
    # Wait for the daemon to start.
    time.sleep(5)
    #  Usage: deluge-console add -p [destination] [torrent file name]
    print("Adding " + torrent_file_name + " to Deluge.")
    print("Saving to " + downloads_directory)
    os.system(deluge_directory + "deluge-console add -p " + downloads_directory + " " + torrents_directory + "/" + torrent_file_name)
    # Initialize the value of torrent_status to ensure it executes the while loop at least once.
    torrent_status = "Downloading"
    counter = 0
    while ("Downloading" in torrent_status):
        print("Heartbeat: " + str(counter))
        counter += 1
        # We don't need to check as often as possible, just every few seconds.
        time.sleep(1)
        torrent_info = (str(subprocess.check_output(deluge_directory + "deluge-console info", shell=True)).split("\\n"))
        print("Torrent_info: " + str(torrent_info))
        torrent_status = torrent_info[3].replace("State: ", "")
        torrent_id = torrent_info[2].replace("ID: ", "")
        print("Torrent status: " + torrent_status)
        if ("Error" in torrent_status):
            # If we get a "Error" status, break and return an error.
            print("Deluge reports that the torrent is in an error state.")
            print("Removing failed download and exiting.")
            os.system(deluge_directory + "deluge-console rm " + torrent_id)
            return -1
        elif ("Seeding" in torrent_status):
            print("Download complete.")
            os.system(deluge_directory + "deluge-console rm " + torrent_id)
            shutil.move(os.path.join(torrents_directory, torrent_file_name), completed_torrents)
        elif ("Checking" in torrent_status):
            # Checking seems to usually mean it is done downloading, and pulling the status triggered the check.
            print("Deluge is checking the torrent.")
        elif ("Queued" in torrent_status):
            print("Torrent is queued for download. This shouldn't be the case, as we only download one at a time!")
        elif ("Stalled" in torrent_status):
            print("The torrent currently has insufficent seeders to complete.")
            stall_counter += 1
            if (stall_counter > 3600):
                print("The torrent has been in a stalled state for one hour.")
                print("Aborting download and removing data.")
                os.system(deluge_directory + "deluge-console rm " + torrent_id)
                return -1


# Extract the archives into the extracted directory. We don't want subdirectories.
def extract_VS_archive():
    file_extension = ".zip"
    for file in os.listdir(downloads_directory):
        if file.endswith(file_extension):
            print("Unzipping " + file)
            file_path = str(downloads_directory + "/" + file)
            subprocess.run("unzip -P infected " + file_path + " -d " + extracted_directory, shell=True, check=True)
            os.remove(file_path)


def unfuck_names(path):
    # Stupid python-magic will explode if it encounters a path object with whitespace in the name.
    # This calls a hacky external Bash script to recursivly substitute whitespace with underlines.
    subprocess.run("bash " + scripts_directory + "/remove_whitespace.sh " + path, shell=True, check=True)


def get_mime(file_path):
    get_mime = magic.Magic(mime=True)
    file_mime = get_mime.from_file(file_path)
    return str(file_mime)

def filter_samples(path):
    unfuck_names(path)
    files = os.listdir(path)
    counter = 0
    for file in files:
        if os.path.isdir(file):
            print(file + " is a directory, not a file. Removing.")
            shutil.rmtree(extracted_directory + "/" + file)
        else:
            counter +=1
            print("Sample #" + str(counter))
            print("File " + file)
            file_mime = get_mime(extracted_directory + "/" + file)
            mac_mimes = ["application/x-mach-binary"]
            linux_mimes = ["application/x-sharedlib", "application/x-pie-executable", "application/x-executable"]
            if bool([string for string in mac_mimes if(string in file_mime)]):
                print("Detected macOS sample type " + file_mime + ".")
                shutil.move(os.path.join(extracted_directory, file), mac_samples_directory)
            elif bool([string for string in linux_mimes if(string in file_mime)]):
                print("Detected Linux sample type " + file_mime + ".")
                shutil.move(os.path.join(extracted_directory, file), linux_samples_directory)
            else: # Probably a Windows sample. We don't need it.
                print("Detected sample type " + file_mime + ".")
                os.system("rm " + extracted_directory + "/" + file)


def main():
    file_names = os.listdir(torrents_directory)
    print(file_names)
    for file_name in file_names:
        print(file_name)
        download_torrent_data(file_name)
        extract_VS_archive()
        filter_samples(extracted_directory)


main()
