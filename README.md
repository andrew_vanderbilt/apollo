# Apollo

Apollo is a hacky wrapper around deluge 1.x that automates the download of large collections of torrent files. This script was originally used to create a server swarm to download the entirety of the VirusShare corpus (8TB). It is presented here for historical reasons. The code is hereby released under the public domain, except the included copy of Deluge.
