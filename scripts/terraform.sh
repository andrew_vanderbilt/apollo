# This script terraforms a Debian 10.x host for the VS_Downloader program.
apt update -y
apt upgrade -y
apt install -y python3 python3-pip libarchive-dev unzip deluge deluged deluge-console libffi-dev libmagic1
pip3 install --upgrade pip
pip3 install python-magic
pip3 install libarchive-c